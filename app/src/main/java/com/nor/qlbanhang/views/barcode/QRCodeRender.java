package com.nor.qlbanhang.views.barcode;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QRCodeRender extends AppCompatImageView {
    public QRCodeRender(Context context) {
        super(context);
    }

    public QRCodeRender(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QRCodeRender(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void generate(String o) {
        Bitmap bitmap = QRCodeHelper.newInstance(getContext())
                .setContent(o)
                .setErrorCorrectionLevel(ErrorCorrectionLevel.Q)
                .setMargin(2)
                .getQRCOde();
        setImageBitmap(bitmap);
    }
}
