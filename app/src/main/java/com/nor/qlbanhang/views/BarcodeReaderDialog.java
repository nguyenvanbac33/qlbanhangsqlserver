package com.nor.qlbanhang.views;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.nor.qlbanhang.databinding.DialogBarcodeReaderBinding;
import com.nor.qlbanhang.views.barcode.QRCodeReader;

public class BarcodeReaderDialog extends Dialog implements View.OnClickListener {
    private DialogBarcodeReaderBinding binding;

    public BarcodeReaderDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth);
        binding = DialogBarcodeReaderBinding.inflate(LayoutInflater.from(context));
        setContentView(binding.getRoot());
        binding.btnClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    public void setListener(QRCodeReader.OnScanResultListener listener) {
        binding.bcRender.setListener(listener);
    }
}
