package com.nor.qlbanhang.views.barcode;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.google.zxing.Result;
import com.nor.qlbanhang.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodeReader extends ZXingScannerView implements ZXingScannerView.ResultHandler {
    private OnScanResultListener listener;

    public QRCodeReader(Context context) {
        super(context);
        init();
    }

    public QRCodeReader(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setBorderColor(Color.WHITE);
        setBorderLineLength((int) getContext().getResources().getDimension(R.dimen.dp_24));
        setBorderStrokeWidth((int) getContext().getResources().getDimension(R.dimen.dp_4));
        setLaserEnabled(false);
        setIsBorderCornerRounded(false);
        setSquareViewFinder(true);
        setResultHandler(this);
        setAutoFocus(true);
    }

    @Override
    public void handleResult(Result result) {
        stopCamera();
        if (listener != null) {
            listener.onScanResult(result.getText().trim());
        }
    }

    public void setListener(OnScanResultListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        Log.e(getClass().getSimpleName(), "onVisibilityChanged");
        if (visibility == VISIBLE){
            startCamera();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.e(getClass().getSimpleName(), "onDetachedFromWindow");
        stopCamera();
    }

    @Override
    public void onVisibilityAggregated(boolean isVisible) {
        super.onVisibilityAggregated(isVisible);
        if (isVisible) {
            startCamera();
        } else {
            stopCamera();
        }
    }

    public interface OnScanResultListener {
        void onScanResult(String result);
    }
}
