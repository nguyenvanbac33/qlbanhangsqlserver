package com.nor.qlbanhang.views;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.nor.qlbanhang.databinding.DialogBarcodeRenderBinding;

public class BarcodeRenderDialog extends Dialog implements View.OnClickListener {
    private DialogBarcodeRenderBinding binding;
    public BarcodeRenderDialog(@NonNull Context context) {
        super(context);
        binding = DialogBarcodeRenderBinding.inflate(LayoutInflater.from(context));
        setContentView(binding.getRoot());
        binding.btnClose.setOnClickListener(this);
    }

    public void show(String id) {
        binding.tvCustomer.setText(id);
        binding.bcRender.generate(id);
        show();
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
