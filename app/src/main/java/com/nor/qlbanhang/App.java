package com.nor.qlbanhang;

import android.app.Application;

import com.nor.qlbanhang.model.User;

public class App extends Application {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
