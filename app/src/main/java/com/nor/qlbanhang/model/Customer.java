package com.nor.qlbanhang.model;

public class Customer extends BaseModel{
    @TableInfo(fieldName = "ID_Cust")
    private String id;
    @TableInfo(fieldName = "Custname")
    private String name;
    @TableInfo(fieldName = "Name")
    private String kiotName;
    @TableInfo(fieldName = "Address")
    private String kiotAddress;
    @TableInfo(fieldName = "Price")
    private long price;
    @TableInfo(fieldName = "ProductName")
    private String productName;

    @TableInfo(fieldName = "Quantity")
    private long quantity;
    @TableInfo(fieldName = "Total")
    private long total;
    @TableInfo(fieldName = "Seri")
    private String seri;
    @TableInfo(fieldName = "UpdateDay")
    private String day;
    @TableInfo(fieldName = "Paid")
    private boolean paid;

    private long quantityEdit;
    private String seriEdit;
    private boolean paidEdit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKiotName() {
        return kiotName;
    }

    public void setKiotName(String kiotName) {
        this.kiotName = kiotName;
    }

    public String getKiotAddress() {
        return kiotAddress;
    }

    public void setKiotAddress(String kiotAddress) {
        this.kiotAddress = kiotAddress;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getSeri() {
        return seri;
    }

    public void setSeri(String seri) {
        this.seri = seri;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public long getQuantityEdit() {
        return quantityEdit;
    }

    public void setQuantityEdit(long quantityEdit) {
        this.quantityEdit = quantityEdit;
    }

    public String getSeriEdit() {
        return seriEdit;
    }

    public void setSeriEdit(String seriEdit) {
        this.seriEdit = seriEdit;
    }

    public boolean isPaidEdit() {
        return paidEdit;
    }

    public void setPaidEdit(boolean paidEdit) {
        this.paidEdit = paidEdit;
    }
}
