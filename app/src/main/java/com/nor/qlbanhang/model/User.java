package com.nor.qlbanhang.model;

public class User extends BaseModel{
    @TableInfo(fieldName = "ID")
    private String id;
    @TableInfo(fieldName = "Username")
    private String name;
    @TableInfo(fieldName = "Password")
    private String password;
    @TableInfo(fieldName = "Tel")
    private String tel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
