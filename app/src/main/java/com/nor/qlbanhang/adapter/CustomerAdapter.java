package com.nor.qlbanhang.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nor.qlbanhang.R;
import com.nor.qlbanhang.databinding.ItemCustomerBinding;
import com.nor.qlbanhang.model.Customer;
import com.nor.qlbanhang.views.BarcodeRenderDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomerHolder> {
    private ArrayList<Customer> data;
    private LayoutInflater inflater;
    private BarcodeRenderDialog renderDialog;
    private boolean isEdit = false;

    public CustomerAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        renderDialog = new BarcodeRenderDialog(inflater.getContext());
    }

    public void setData(ArrayList<Customer> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ArrayList<Customer> getData() {
        return data;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomerHolder(ItemCustomerBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                renderDialog.show(data.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class CustomerHolder extends RecyclerView.ViewHolder {
        private ItemCustomerBinding binding;
        public CustomerHolder(ItemCustomerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(final Customer item) {
            if (item.isPaid()) {
                binding.panelContainer.setBackgroundResource(R.color.colorPaid);
            } else {
                binding.panelContainer.setBackgroundColor(Color.WHITE);
            }
            binding.tvName.setText(item.getName());
            binding.tvKiotName.setText(item.getKiotName());
            binding.tvKiotAddress.setText(item.getKiotAddress());
            binding.tvId.setText(item.getId());
            binding.tvDay.setText(item.getDay());
            binding.tvSeri.setText(item.getSeri());
            binding.tvProductName.setText(item.getProductName());
            binding.tvPrice.setText(formatMoney(item.getPrice()));
            binding.tvQuantity.setText(String.valueOf(item.getQuantity()));
            binding.tvTotal.setText(String.valueOf(item.getTotal()));

            binding.lnEditQuantity.setVisibility(isEdit ? View.VISIBLE : View.GONE);
            binding.edtQuantity.setText(item.getQuantity() + "");
            item.setQuantityEdit(item.getQuantity());
            binding.edtQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().isEmpty()) {
                        item.setQuantityEdit(0);
                    } else {
                        item.setQuantityEdit(Integer.parseInt(s.toString()));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            binding.edtSeri.setText(item.getSeri());
            item.setSeriEdit(item.getSeri());
            binding.edtSeri.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    item.setSeriEdit(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            binding.cbPaid.setChecked(item.isPaid());
            item.setPaidEdit(item.isPaid());
            binding.cbPaid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    item.setPaidEdit(isChecked);
                }
            });
        }

        private String formatMoney(long number) {
            NumberFormat formatter = new DecimalFormat("#,###");
            return formatter.format(number);
        }
    }
}
