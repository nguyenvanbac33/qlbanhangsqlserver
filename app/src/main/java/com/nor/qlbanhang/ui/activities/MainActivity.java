package com.nor.qlbanhang.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nor.qlbanhang.R;
import com.nor.qlbanhang.databinding.ActivityMainBinding;
import com.nor.qlbanhang.ui.fragment.CustomerFragment;
import com.nor.qlbanhang.ui.fragment.ProfileFragment;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ProfileFragment fmProfile = new ProfileFragment();
    private CustomerFragment fmCustomer = new CustomerFragment();
    private MenuItem menuItem;
    private Menu menu;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpFragment();
        showFragment(fmCustomer);
        binding.nav.setOnNavigationItemSelectedListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[] {Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    private void setUpFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fmProfile);
        transaction.add(R.id.container, fmCustomer);
        transaction.commit();
    }

    private void showFragment(Fragment fmShow) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        transaction.hide(fmProfile);
        transaction.hide(fmCustomer);
        transaction.show(fmShow);
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        boolean isCustomer = item.getItemId() == R.id.tab_customer;
        menuItem.setVisible(isCustomer);
        switch (item.getItemId()) {
            case R.id.tab_customer:
                showFragment(fmCustomer);
                break;
            case R.id.tab_profile:
                showFragment(fmProfile);
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        this.menu = menu;
        menuItem = menu.findItem(R.id.menu_edit);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        onMenuEditClicked(item.getItemId() == R.id.menu_edit);
        return super.onOptionsItemSelected(item);
    }

    public void onMenuEditClicked(boolean isEdit) {
        menuItem.setVisible(false);
        if (isEdit) {
            menuItem = menu.findItem(R.id.menu_cancel);
            fmCustomer.setEdit(true);
        } else {
            menuItem = menu.findItem(R.id.menu_edit);
            fmCustomer.setEdit(false);
        }
        menuItem.setVisible(true);
    }
}
