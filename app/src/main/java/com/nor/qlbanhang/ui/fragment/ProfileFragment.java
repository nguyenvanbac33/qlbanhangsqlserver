package com.nor.qlbanhang.ui.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.nor.qlbanhang.App;
import com.nor.qlbanhang.R;
import com.nor.qlbanhang.databinding.UiProfileBinding;
import com.nor.qlbanhang.model.User;

public class ProfileFragment extends BaseFragment<UiProfileBinding> {
    @Override
    protected int getLayoutId() {
        return R.layout.ui_profile;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        User user = ((App) getContext().getApplicationContext()).getUser();
        binding.edtId.setText(user.getId());
        binding.edtName.setText(user.getName());
        binding.edtTel.setText(user.getTel());
        binding.edtPassword.setText(user.getPassword());
    }
}
