package com.nor.qlbanhang.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.nor.qlbanhang.App;
import com.nor.qlbanhang.R;
import com.nor.qlbanhang.adapter.CustomerAdapter;
import com.nor.qlbanhang.dao.JDBCController;
import com.nor.qlbanhang.dao.SqlExecute;
import com.nor.qlbanhang.databinding.UiCustomerBinding;
import com.nor.qlbanhang.databinding.UiProfileBinding;
import com.nor.qlbanhang.model.Customer;
import com.nor.qlbanhang.model.User;
import com.nor.qlbanhang.print.PrintActivity;
import com.nor.qlbanhang.ui.activities.MainActivity;
import com.nor.qlbanhang.views.BarcodeReaderDialog;
import com.nor.qlbanhang.views.barcode.QRCodeReader;

import java.util.ArrayList;

public class CustomerFragment extends BaseFragment<UiCustomerBinding> implements AdapterView.OnItemSelectedListener, TextWatcher, View.OnClickListener, QRCodeReader.OnScanResultListener {
    private CustomerAdapter adapter;
    private BarcodeReaderDialog reader;

    @Override
    protected int getLayoutId() {
        return R.layout.ui_customer;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.spTime.setOnItemSelectedListener(this);
        binding.edtSearch.addTextChangedListener(this);
        adapter = new CustomerAdapter(getLayoutInflater());
        binding.lvCustomer.setAdapter(adapter);
        loadData();
        binding.imScan.setOnClickListener(this);
    }

    private void loadData() {
        ArrayList<Customer> customers = JDBCController.getData(Customer.class, SqlExecute.getSqlCustomer(binding.spTime.getSelectedItem().toString(), binding.edtSearch.getText().toString()));
        adapter.setData(customers);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        loadData();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        loadData();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.im_scan:
                reader = new BarcodeReaderDialog(getContext());
                reader.setListener(this);
                reader.show();
                break;
            case R.id.btn_update:
                doUpdate();
                break;
            case R.id.btn_print:
                doPrint();
                break;
        }
    }

    private void doPrint() {
        ArrayList<Customer> data = getCustomerEdited();
        if (data.isEmpty()) {
            Toast.makeText(getContext(), "No data change", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(getContext(), PrintActivity.class);
        intent.putExtra(Customer.class.getSimpleName(), data);
        startActivity(intent);
    }

    private void doUpdate() {
        ArrayList<Customer> customers = getCustomerEdited();
        if (customers.isEmpty()) {
            ((MainActivity) getActivity()).onMenuEditClicked(false);
            return;
        }
        String sql = "";
        String month = binding.spTime.getSelectedItem().toString();
        for (Customer customer : customers) {
            sql += SqlExecute.getSqlUpdateCustomer(customer, month);
        }
        boolean result = JDBCController.execute(sql);
        if (result) {
            Toast.makeText(getContext(), "Update success", Toast.LENGTH_SHORT).show();
            ((MainActivity) getActivity()).onMenuEditClicked(false);
            loadData();
        } else {
            Toast.makeText(getContext(), "Update false", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onScanResult(String result) {
        reader.dismiss();
        binding.edtSearch.setText(result);
    }

    public void setEdit(boolean isEdit) {
        adapter.setEdit(isEdit);
        binding.panelFilter.setVisibility(isEdit ? View.GONE : View.VISIBLE);
        binding.panelUpdate.setVisibility(!isEdit ? View.GONE : View.VISIBLE);
        binding.btnUpdate.setOnClickListener(this);
        binding.btnPrint.setOnClickListener(this);
    }

    private ArrayList<Customer> getCustomerEdited() {
        ArrayList<Customer> customers = new ArrayList<>();
        for (Customer customer : adapter.getData()) {
            if (customer.getQuantityEdit() != customer.getQuantity()
                    || (customer.getSeriEdit() != null && customer.getSeriEdit().equals(customer.getSeri()) == false)
                    || customer.isPaidEdit() != customer.isPaid()
            ) {
                customers.add(customer);
            }
        }
        return customers;
    }
}
