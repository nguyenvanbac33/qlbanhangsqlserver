package com.nor.qlbanhang.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.nor.qlbanhang.App;
import com.nor.qlbanhang.R;
import com.nor.qlbanhang.dao.JDBCController;
import com.nor.qlbanhang.dao.SqlExecute;
import com.nor.qlbanhang.databinding.ActivityLoginBinding;
import com.nor.qlbanhang.model.User;

import java.util.ArrayList;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements View.OnClickListener {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String userName = binding.edtUserName.getText().toString();
        String password = binding.editPassword.getText().toString();
        if (userName.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "User name or password is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<User> users = JDBCController.getData(User.class, SqlExecute.getSqlLogin(userName, password));
        if (users.size() == 0) {
            Toast.makeText(this, "User name or password incorrect", Toast.LENGTH_SHORT).show();
            return;
        }
        ((App) getApplicationContext()).setUser(users.get(0));
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
